# Call Now Button

Call Now Button places button to the bottom of the screen which is
only visible for your mobile visitors.
Because your mobile visitors already have a phone in their hands this
module will allow them to call you with one simple touch of the button.
No more navigating to the contact page and no more complicated
copy/pasting or memorizing the phone number!

For a full description of the module, visit the
[project page](https://www.drupal.org/project/call_now_button).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/call_now_button).


## Contents of this file

- Requirements
- Installation
- Configuration
- Maintainers


## Requirements

This module requires no modules outside of Drupal core.


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

This module does not have any configuration.


## Maintainers

- Neslee Canil Pinto - [Neslee Canil Pinto](https://www.drupal.org/u/neslee-canil-pinto)
